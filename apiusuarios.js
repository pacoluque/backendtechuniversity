var express = require('express')
var fs = require('fs')
var bodyParser = require('body-parser')
var app = express()
var port = process.env.PORT || 4000


app.use(bodyParser.json())

app.listen(port)
console.log("API escuchando en el puerto "+port)


/****** PETICION GET *******************/

app.get("/apitechu/V1", function(req, res)
  {
  //console.log(req)
  res.send({"mensaje":"Bienvenido a mi primera API"})
})
