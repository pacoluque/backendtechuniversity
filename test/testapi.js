var moca = require('mocha')
var chai = require('chai')
var chaiHttp = require('chai-http')
var miServer = require('../server.js')

chai.use(chaiHttp)

var should = chai.should()

describe('Test Google', () =>
  {
  it('Google funciona', (done) =>
    {
    chai.request('http://www.google.es')
      .get('/')
      .end((err, res) =>
      {
      //console.log(res)
      res.should.have.status(200)
      done()
      })
    })
  })


  describe('Test Conectividad y Usuarios miApi', () =>
    {
    it('Raíz OK', (done) =>
      {
      chai.request('http://localhost:4000')
        .get('/apitechu/V1')
        .end((err, res) =>
        {
        //console.log(res)
        res.should.have.status(200)
        res.body.mensaje.should.be.eql('Bienvenido a mi primera API')
        done()
        })
      })
    it('Raíz OK', (done) =>
      {
      chai.request('http://localhost:4000')
        .get('/apitechu/V1/usuarios')
        .end((err, res) =>
        {
        //console.log(res)
        res.should.have.status(200)
        res.body.should.be.a('array')

        //console.log('mail '+res.body[10].email);
        for (var i = 0; i < res.body.length; i++)
          {
          res.body[i].should.have.property('email')
          res.body[i].should.have.property('password')
          }
        //res.body.mensaje.should.be.eql('Bienvenido a mi primera API')
        done()
        })
      })
    })
